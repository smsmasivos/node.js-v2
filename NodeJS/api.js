const fs = require('fs');
const path = require("path");
const express = require("express");
const request = require('request');

const app = express();

const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "..", "..", "..")));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
  
app.post('/sendMessage', async (req, response) => {
    var data = req.body;//Variable to get the data od the frontend

    var inforRequest = {
        "message": data.message,
        "numbers": data.addressee,
        "country_code": data.country,
        "sandbox": parseInt(data.sandbox)
    };

    if(data.name != null && data.name != ""){
        inforRequest["name"] = data.name;
    }

    var targetURL = 'https://api.smsmasivos.com.mx/sms/send'
    await request.post({
                        url: targetURL,
                        headers: {
                            "apikey": data.apikey
                        },
                        form: inforRequest
            }, (err, res, body) => {
                if(err){
                    response.json({
                        "success": false,
                        "message": "Error to get you api key"
                    }).status(400);
    
                    return;
                }

                response.json( JSON.parse( body ) ).status(200)
            });
})



app.post('/getCredits', async (req, response) => {
    var data = req.body;//Variable to get the data od the frontend
    var targetURL = 'https://api.smsmasivos.com.mx/credits/consult'
    await request.post({
                        url: targetURL,
                        headers: {
                            "apikey": data.apikey
                        }
    }, (err, res, body) => {
        if(err){
            response.json({
                "success": false,
                "message": "Error to get you api key"
            }).status(400);
            return;
        }

        response.json( JSON.parse( body ) ).status(200)
  });
})


app.post("/authRegister", async (req, response) => {
    var data = req.body;//Variable to get the data od the frontend
    var targetURL = `https://api.smsmasivos.com.mx/protected/${data.format}/phones/verification/start`
    
    var infoRequest = {
        "phone_number": data.addressee,
        "country_code": data.country
    };

    if(data.name != null && data.name != ""){
        infoRequest["company"] = data.name
    }

    if(data.digits != null && data.digits != ""){
        infoRequest["code_length"] = data.digits
    }

    await request.post({
                        url: targetURL,
                        headers: {
                            "apikey": data.apikey
                        },
                        form: infoRequest
    }, (err, res, body) => {
        
        if(err){
            response.json({
                "success": false,
                "message": "Error to get you api key"
            }).status(400);
            return;
        }
        
        if(data.format == "json") {
            response.json( JSON.parse( body ) ).status(200)
        }else{
            response.json( body ).status(200)
        }

    });
})


app.post('/authValidation', async (req, response) => {
    var data = req.body;//Variable to get the data od the frontend
    var targetURL = `https://api.smsmasivos.com.mx/protected/${data.format}/phones/verification/check`

    var infoRequest = {
        "phone_number": data.dest,
        "verification_code": data.code
    }

    await request.post({
                        url: targetURL,
                        headers: {
                            "apikey": data.apikey
                        },
                        form: infoRequest
  }, (err, res, body) => {
    if(err){
        response.json({
            "success": false,
            "message": "Error to get you api key"
        }).status(400);
        return;
    }
    
    if(data.format == "json") {
        response.json( JSON.parse( body ) ).status(200)
    }else{
        response.json( body ).status(200)
    }

  }
);
    
})


app.listen(3000)